from neo4j.v1 import GraphDatabase
import sys

class CypherClient:
    """
    The client that execute cypher
    """
    def __init__(self, uri, auth):
        self.driver = GraphDatabase.driver(uri, auth=auth)

    def run_cypher(self, cypher):
        """
        execute single cypher
        :param cypher: the cypher in str
        :return: no return anything at all
        """
        with self.driver.session() as session:
            session.run(cypher).single()

def test():
    """
    test thing out
    :return:
    """
    uri = "bolt://188.166.233.199:7687"
    auth = ("tthien", "neo4jmeow")
    # driver = GraphDatabase.driver(uri, auth=("tthien", "neo4jmeow"))

    client = CypherClient(uri, auth)
    client.run_cypher("CREATE (cat:CAT{name:'PUSHEEN'})")
    print('done')


if __name__=="__main__":
    uri = "bolt://188.166.233.199:7687"
    auth = ("tthien", "neo4jmeow")
    # driver = GraphDatabase.driver(uri, auth=("tthien", "neo4jmeow"))
    counter = 0

    if len(sys.argv) < 2:
        test()
    else:
        client = CypherClient(uri, auth)
        infile = sys.argv[1]
        for line in open(infile):
            # print(line)
            client.run_cypher(line)
            counter+=1
            if counter % 100 == 0 or counter < 100:
                print(counter)
    print('done')

"""
python ex_cypher_file.py outcypher.txt 
"""