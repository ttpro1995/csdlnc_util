import openpyxl
import unidecode
import requests
from util.vn import remove_tonemark_token



class ProductRow:
    def __init__(self, xl_row):
        self.product_type = xl_row[0].value
        self.group_str = xl_row[1].value.lower()
        self.product_code = xl_row[2].value.lower()
        self.product_name = xl_row[3].value
        self.product_price = xl_row[4].value
        self.base_url = "http://xuongmaythienphuc.vn/"
        self.url = self.base_url + "component/products/" + unidecode.unidecode(self.product_name).replace(' ', '-').lower() + ".html"

        # parse group to group list
        self.group_list = self.group_str.split(">>")

    def __str__(self):
        str_list = []

        s1 = "Loai san pham " + self.product_type
        s2 = "Ma san pham " + self.product_code
        s3 = "Ten san pham  " + self.product_name
        s4 = "gia " + str(self.product_price)
        s5 = "url " + str(self.url)
        s6 = "url"  + str(self.group_list)
        str_list.append(s1)
        str_list.append(s2)
        str_list.append(s3)
        str_list.append(s4)
        str_list.append(s5)
        str_list.append(str(s6))
        return  '\n'.join(str_list)

    def to_category_cypher(self):
        """
        generate cypher to MERGE category
        :return: list of cyphers, one for per category
        """
        categoryCypher = []
        templateCategoryCypher =  "MERGE (category:CATEGORY{zPropz})"
        templateName = "name: 'zNamez', normalized_name: 'zNorNamez' "
        for category in self.group_list:
            n_category = remove_tonemark_token(category).lower()
            cypher = templateCategoryCypher.replace("zPropz", templateName.replace("zNamez", category).replace("zNorNamez", n_category))
            categoryCypher.append(cypher)
        return categoryCypher

    def to_product_cypher(self):
        """
        generate cypher to MERGE product
        :return:
        """
        templateProductCypher = "MERGE (product:PRODUCT{ name:'zProductz', url: 'zUrlz', normalized_name: 'zNorProductz'})"
        cypher = templateProductCypher
        normalized_product_name = remove_tonemark_token(self.product_name).lower()

        cypher = cypher.replace("zProductz", self.product_name)
        cypher = cypher.replace("zNorProductz", normalized_product_name)
        cypher = cypher.replace("zUrlz", self.url)

        return cypher

    def to_item_cypher(self):
        """
        Generate cypher to MERGE item
        :return:
        """

        templateItemCypher = "MERGE (item:ITEM{code:'zCodez', price: zPricez})"

        cypher = templateItemCypher
        cypher = cypher.replace("zCodez", self.product_code)
        cypher = cypher.replace("zPricez", str(self.product_price))
        return cypher

    def to_list_cypher(self):
        cypher_list = self.to_category_cypher()
        cypher_list.append(self.to_product_cypher())
        cypher_list.append(self.to_item_cypher())
        return cypher_list

    def to_relationship_cypher(self):
        templateRel1 = "MATCH (p:PRODUCT),(i:ITEM) " \
                      "WHERE  p.name = 'zPNamez' AND i.code = 'zCodez' " \
                      "MERGE (p)-[:HAS]->(i)"

        templateRel2 = "MATCH (c:CATEGORY), (p:PRODUCT) " \
                       "WHERE c.name = 'zCNamez' AND p.name = 'zPNamez'" \
                       "MERGE (c)<-[:BELONG_TO]-(p)"

        ret = []
        for group in self.group_list:
            cypher2 = templateRel2
            cypher2 = cypher2.replace("zCNamez", group)
            cypher2 = cypher2.replace("zPNamez", self.product_name)
            ret.append(cypher2)

        cypher1 = templateRel1
        cypher1 = cypher1.replace("zPNamez", self.product_name)
        cypher1 = cypher1.replace("zCodez", self.product_code)

        ret.append(cypher1)
        return ret

    def check_valid_url(self):
        r = requests.get(self.url)
        if (r.status_code==200):
            return True
        return False


def import_from_ms_xlsx(file_path):
    wb = openpyxl.load_workbook(filename=file_path, read_only=True)
    ws = wb['DanhSachSanPham']
    outfile = open("outcypher.txt", 'a+')
    group_list = []
    counter = 0
    for row in ws.rows:
        product_row = ProductRow(row)
        counter += 1
        if product_row.check_valid_url():
            print(counter)
            cypherlist = product_row.to_list_cypher()
            cypherlist += product_row.to_relationship_cypher()
            for cypher in cypherlist:
                outfile.write(cypher + "\n")
    outfile.close()

    group_set = set(group_list)
    print(group_set)


if __name__ == "__main__":
    import_from_ms_xlsx("data/DanhSachSanPham.xlsx")
    pass
