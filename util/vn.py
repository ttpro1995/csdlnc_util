def _bodau(a):
    """
    remove vietnamese tonemark
    :param a:
    :return:
    """
    Char = "àảãáạăằẳẵắặâầẩẫấậòỏõóọôồổỗốộơờởỡớợèẻẽéẹêềểễếệùủũúụưừửữứựìỉĩíịỳỷỹýỵđ"
    kqua = "aAoOeEuUiIyYdD"
    a+=""
    lower = False
    b = a.lower()
    if (a==b): lower = True
    else : lower = False
    i = Char.find(b)
    if i==-1 :return a
    if i<=16 :return kqua[0+1-lower]
    if i<=33 :return kqua[2+1-lower]
    if i<=44 :return kqua[4+1-lower]
    if i<=55 :return kqua[6+1-lower]
    if i<=60 :return kqua[8+1-lower]
    if i<=65 :return kqua[10+1-lower]
    if i<=66 :return kqua[12+1-lower]


def remove_tonemark_token(inStr):
    """
    Remove vietnamese tonemark from a token
    :param inStr:
    :return:
    """
    tok = inStr.rstrip()
    tok = ''.join([_bodau(a) for a in tok])
    return tok


def normalize_vn_key(inkey):
    """
    normalize vn key
    Hiển thị màu sắc  => hien_thi_mau_sac
    :param inkey: str of key
    :return: str of normalize key
    """
    removed_tonemark = remove_tonemark_token(inkey)
    underline = removed_tonemark.replace(" ", "_").replace("/","").replace("(","").replace(")", "").replace("-", "_")
    lowercase = underline.lower()
    result = lowercase
    return result


def normalize_neo4j_str(in_str):
    """
    remove invalid token in str
    :param in_str:
    :return:
    """
    return in_str.replace("'", "").replace('"', "").replace("\\"," ").replace("/", " ").replace("\n", "  ").replace("\r", " ")